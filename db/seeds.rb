# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
chimura = User.new(
  name: "ちむら",
  email: "chimura@chimura.chimura",
  admin: true,
  password: "password",
  password_confirmation: "password"
)
chimura.save
tarou = User.new(
  name: "tarou",
  email: "tarou@chimura.chimura",
  password: "password",
  password_confirmation: "password"
)
tarou.save
ryohei = User.new(
  name: "ryohei",
  email: "ryohei@chimura.chimura",
  password: "password",
  password_confirmation: "password"
)
ryohei.save

masao = Author.create(name: "大武政男")
chika = Author.create(name: "羽海野チカ")
koji = Author.create(name: "森恒二")

hina1 = Book.create(
  author_id: masao.id,
  name: "ヒナまつり(1)",
  cover: open("#{Rails.root}/public/uploads/cover/hina1.jpg")
)
Review.create(
  book_id: hina1.id,
  user_id: chimura.id,
  score: 4,
  comment: "めっちゃ面白かったですわ"
)
Review.create(
  book_id: hina1.id,
  user_id: tarou.id,
  score: 5,
  comment: "めっちゃ面白かったですね"
)

hina2 = Book.create(
  author_id: masao.id,
  name: "ヒナまつり(2)"
)
Review.create(
  book_id: hina2.id,
  user_id: chimura.id,
  score: 4,
  comment: "めっちゃ面白かったですわ"
)
Review.create(
  book_id: hina2.id,
  user_id: tarou.id,
  score: 5,
  comment: "めっちゃ面白かったですね"
)

hina3 = Book.create(
  author_id: masao.id,
  name: "ヒナまつり(3)",
  cover: open("#{Rails.root}/public/uploads/cover/hina3.jpg")
)
toui = Book.create(
  author_id: masao.id,
  name: "東京発異世界行き",
  cover: open("#{Rails.root}/public/uploads/cover/toui.jpg")
)

hachikuro1 = Book.create(
  author_id: chika.id,
  name: "ハチミツとクローバー(1)",
  cover: open("#{Rails.root}/public/uploads/cover/hachikuro1.jpg")
)
Review.create(
  book_id: hachikuro1.id,
  user_id: chimura.id,
  score: 3,
  comment: "面白かったです"
)

hachikuro2 = Book.create(
  author_id: chika.id,
  name: "ハチミツとクローバー(2)",
  cover: open("#{Rails.root}/public/uploads/cover/hachikuro2.jpg")
)

sanlion1 = Book.create(
  author_id: chika.id,
  name: "3月のライオン(1)",
  cover: open("#{Rails.root}/public/uploads/cover/sanlion1.jpg")
)
sanlion2 = Book.create(
  author_id: chika.id,
  name: "3月のライオン(2)"
)

jisajima1 = Book.create(
  author_id: koji.id,
  name: "自殺島(1)",
  cover: open("#{Rails.root}/public/uploads/cover/jisajima1.jpg")
)
jisajima2 = Book.create(
  author_id: koji.id,
  name: "自殺島(2)",
  cover: open("#{Rails.root}/public/uploads/cover/jisajima2.jpg")
)
jisajima3 = Book.create(
  author_id: koji.id,
  name: "自殺島(3)",
  cover: open("#{Rails.root}/public/uploads/cover/jisajima3.jpg")
)