class AddIndexToReviewsBookUser < ActiveRecord::Migration[5.2]
  def change
    add_index :reviews, [:book_id, :user_id], unique: true
  end
end
