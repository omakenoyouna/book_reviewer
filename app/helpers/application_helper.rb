module ApplicationHelper

  def is_admin?
    return user_signed_in? && current_user.admin
  end

  def ratystars(score=0, display_score=false, max=5)
    content_tag(:div, :class=>"d-inline") do
      if display_score then
        concat content_tag(:span, "%#.03g" % score.to_s + "  ")
      end
      for i in 1..max do
        if score >= i then
          concat content_tag(:img, "&nbsp".html_safe, :src=>"/assets/star-on.png")
        elsif score + 0.5 >= i then
          concat content_tag(:img, "&nbsp".html_safe, :src=>"/assets/star-half.png")
        else
          concat content_tag(:img, "&nbsp".html_safe, :src=>"/assets/star-off.png")
        end
      end
    end
  end

end
