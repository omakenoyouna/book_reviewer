class UsersController < ApplicationController
  before_action :set_user, only: [:show]

  def index
  end

  def show
  end
  
  private
  
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:user_id)
    end
  
end
