class Review < ApplicationRecord
  belongs_to :book
  belongs_to :user
  validates :score, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 5 } 
  validates :comment, length: { in: 1..400 }
  validates :user, presence: true, uniqueness: {scope: :book}
end
