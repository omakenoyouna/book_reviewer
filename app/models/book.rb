class Book < ApplicationRecord
  belongs_to :author
  has_many :reviews
  paginates_per 8
  mount_uploader :cover, CoverUploader
  validates :name, presence: true, uniqueness: {scope: :author}
  validates :author, presence: true
  validate :cover_size
  
  def score
    self.reviews.average(:score).floor(2)
  end
  
  private
  
    def cover_size
      if cover.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end

end